import java.util.NoSuchElementException;
import java.util.Queue;

public class LinkedQueue implements QueueClass {
private class Node{
	
	public Object data;
	public Node next;
	int size = 0;
	public Node(Object data, Node next){
		this.data = data;
		this.next = next;
	}
}
private Node head = null;
private Node tail = null;

public Object enqueue(Object item){
	Node newNode = new Node(item, null);

	if(isEmpty())
	{ 
		head = newNode;
	} 
	
	else
	{
		tail.next = newNode;	
	}
	tail = newNode;
	return item.toString();
}
public Object dequeue() {
    if (isEmpty()) {
        throw new NoSuchElementException();
    }
    Object item = head.data;
    if (tail == head) {
        tail = null;
    }
    head = head.next;
    return item;
}
public Object peek() {
    if (head == null) {
        throw new NoSuchElementException();
    }
    return head.data;
}
public boolean isEmpty() {
    return head == null;
}

public int size() {
    int count = 0;
    for (Node node = head; node != null; node = node.next) {
        count++;
    }
    return count;
}


public static void printMe(LinkedQueue q){
//	if(q.isEmpty())
//	{
//		System.out.println("Queue is empty");
//	}
//	else{
		for(int i = 0; i < q.size(); i++)
		{
			System.out.println("HI "+(q.dequeue()));
		}
	}
}


