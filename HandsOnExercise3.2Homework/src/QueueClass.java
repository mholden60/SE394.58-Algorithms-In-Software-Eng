
public interface QueueClass {

	
	Object enqueue(Object item);
	
	Object dequeue();
	
	Object peek();
	
	int size();
	
	boolean isEmpty();
	
	
}
