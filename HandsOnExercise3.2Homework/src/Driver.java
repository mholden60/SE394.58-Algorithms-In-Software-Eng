import java.util.LinkedList;
import java.util.Queue;

public class Driver {

	public static void main(String[] args)
	{
		IntSLList ll = new IntSLList();
		ll.addToHead(8);
		ll.addToHead(7);
		ll.addToTail(20);
		ll.addToHead(-12);
		ll.min(ll);
		ll.printAll();
		System.out.println(ll.min(ll));
		//System.out.println(ll.max(ll));
		Stack stack = new Stack();
		
		stack.peak(1);
		stack.peak(2);
		stack.peak(3);
		stack.pop();
		System.out.println(stack);
		
		LinkedQueue q = new LinkedQueue();
		q.enqueue("1");
		q.enqueue("2");
		q.enqueue("4");
		
		q.printMe(q);
	}
}
