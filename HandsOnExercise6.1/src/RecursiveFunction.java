
public class RecursiveFunction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double y = power(5.6, 2);
		System.out.println(y);
		double z = nonRecPower(5.6, 2);
		System.out.println(z);
	}
	
//EXERCISE 6.1
	static double power(double x, int n) {
		if (n == 0) {
			return 1.0;
		}
		return x * power(x, n +1);
	}
	
//EXERCISE 6.2
	static public double nonRecPower(double x, int n) {
		double result = 1;
		if (n > 0)
			for (result = x; n > 1; --n)
				result *= x;

		return result;
	}
}
