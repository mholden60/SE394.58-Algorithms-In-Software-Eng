import java.util.HashSet;

public class IntSLList {
protected IntSLLNode head, tail;

public IntSLList(){
	head = tail = null;
}

public boolean isEmpty(){
	return head == null;
}

public void addToHead(int el){
	head = new IntSLLNode(el, head);
	if(tail == null)
		tail = head;
}

public void addToTail(int el)
{
	if(!isEmpty())
	{
		tail.next = new IntSLLNode(el);
		tail = tail.next;
	}
	else head = tail = new IntSLLNode(el);
}

public int deleteFromHead()
{
	int el = head.info;
	if(head == tail)
		head = tail = null;
	else head = head.next;
	return el;
}

public int deleteFromTail(){
	int el = tail.info;
	if(head == tail)
	{
		head = tail= null;
	}
	else{
		IntSLLNode tmp;
		for(tmp = head; tmp.next !=tail; tmp = tmp.next);
		tail = tmp;
		tail.next = null;
	}
	return el;
		}

public void printAll(){

	for (IntSLLNode tmp = head; tmp!= null; tmp = tmp.next)
		System.out.println(tmp.info + " ");
	}

public boolean isInList(int el)
{
	IntSLLNode tmp;
	for(tmp = head; tmp!= null && tmp.info !=el; tmp = tmp.next);
	return tmp != null;
}

public void delete(int el){
	if(!isEmpty())
	{
		if(head == tail&& el == head.info)
			head = tail= null;
		else if(el == head.info)
			head = head.next;
		else{
			IntSLLNode pred, tmp;
			for(pred = head, tmp = head.next; tmp != null && tmp.info != el; pred = pred.next, tmp = tmp.next);
			if(tmp != null){
				pred.next = tmp.next;
				if( tmp == tail)tail = pred;
			}
		}
	}
}

public void sum(){
	int sum = 0;
	
	for(IntSLLNode temp = head; temp != null; temp = temp.next){
		sum += temp.info;
	}
	System.out.println("Sum: "+ sum);
}

public void max(){
	int large = 0;
	
	for (IntSLLNode temp = head; temp != null; temp = temp.next){
		if(large < temp.info){
			large = temp.info;
		}
	}
	System.out.println("Largest Number: " + large);
}

public int getMaxNumber(){
int maxNumber = 0;

for(IntSLLNode temp = head; temp != null; temp = temp.next){
	if(maxNumber < temp.info){
		maxNumber = temp.info;
	}
}
return maxNumber;
}

public void min(){
	int min = 0;
	min = this.getMaxNumber();
	
	for(IntSLLNode temp = head; temp != null; temp = temp.next){
		if(temp.info < min){
			min = temp.info;
		}
	}
	System.out.println("Smallest Number: " + min);
}

public void removeDuplicates(){

	HashSet<Integer> hashSet = new HashSet<>();
	IntSLLNode current = head;
	IntSLLNode prev = null;
	
	while(current != null){
		if(hashSet.contains(current.info)){
			prev.next = current.next;
		}
		else{
			hashSet.add(current.info);
			prev = current;
		}
	}
	current = current.next;
	
}

public void removeMedian(){
	if(!isEmpty()){
		bubbleSort();
		int listSize = 0;
		for(IntSLLNode temp = head; temp != temp.next; listSize++){
			int halfListSize = listSize / 2;
		}
		for(IntSLLNode pred = head, temp = head.next; listSize > 0; pred = pred.next, temp = temp.next, listSize--){
			if(listSize % 2 ==0){
				if(listSize == 2){
					pred.next.next = temp.next.next;
				}
			}
			else if(listSize == 1){
				pred.next = temp.next;
			}
		}
	}
}

public void bubbleSort(){
	boolean sorted = true;
	if(!isEmpty()&& head != tail){
		while(sorted){
			sorted = false;
			for(IntSLLNode pred = head, temp = head.next; temp != null; pred = pred.next, temp = temp.next){
				if(pred.info > temp.info){
					int large;
					large = pred.info;
					pred.info = temp.info;
					temp.info = large;
					sorted = true;
				}
			}
		}
	}
}
}
