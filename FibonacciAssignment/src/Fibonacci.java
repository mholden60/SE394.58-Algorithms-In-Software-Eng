
public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count = 10;
		int[] feb = new int[count];
		feb[0] = 0;
		feb[1] = 1;
		for (int i = 2; i < count; i++) {
			feb[i] = feb[i - 1] + feb[i - 2];
		}

		for (int i = 0; i < count; i++) {
			System.out.print(count + " " + feb[i] + " ");
		}
	}

	/*
	 * n = 0 01 02 03 04 05 06 07 08 09 10 
	 * Xn = 0 01 01 02 03 05 08 13 21 34 55
	 * 
	 * X(n) = X(n-1) + X(n-2)
	 * 
	 * Example. X8 = X(8-1) + X(8-2) 
	 * X(8) = X(7) + X(6) 
	 * X8 = 21 
	 * X7 = 13 
	 * X6 = 08
	 * 21 = 13 + 8
	 * 
	 */

}
