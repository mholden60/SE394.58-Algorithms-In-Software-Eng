package mHolden_BubbleSort;

import java.util.concurrent.TimeUnit;

public class Run {
	static Employee[] empArray;

	public Employee[] sortAge(Employee[] data){
		int len = data.length;
		for(int i = 1; i < len; i++){
			System.out.println("Pass ="+ i);
			for(int compares = 0; compares < len - i; compares++){
				System.out.println(compares);
				
				if(data[compares].getAge() > data[compares+1].getAge()){
					Employee temp = data[compares];
					data[compares] = data[compares+1];
					data[compares+1] = temp;
				}
			}
		}
		return data;
	}
	public Employee[] sortName(Employee[] data) {
		int len = data.length; // size of array
	
		
		for (int i = 1; i < len; i++) {
			System.out.println("Pass = " + i);
			for (int compares = 0; compares < len- i; compares++) {
				System.out.println(compares);
				
				if (data[compares].getName().compareTo(data[compares+1].getName()) > 0) {
					Employee temp = data[compares];
					data[compares] = data[compares+1];
					data[compares+1] = temp;
				}
			}
		}
		return data;
		
	}
	public Employee[] sortPosition(Employee[] data) {
		int len = data.length; // size of array
	
		
		for (int i = 1; i < len; i++) {
			System.out.println("Pass = " + i);
			for (int compares = 0; compares < len- i; compares++) {
				System.out.println(compares);
				
				if (data[compares].getName().compareTo(data[compares+1].getName()) > 0) {
					Employee temp = data[compares];
					data[compares] = data[compares+1];
					data[compares+1] = temp;
				}
			}
		}
		return data;
		
	}
	public void printData(){
		for(Employee emp : empArray){
			System.out.println("Age = " + emp.getAge() + " Name = "+ emp.getName()+ " Position= "+emp.getPosition());
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
         empArray = new Employee[5];
         Employee emp;
          long startTime = System.nanoTime();
       
         
         empArray[0] = new Employee(35, "Mathew", "Grill");
         empArray[1] = new Employee(36, "Wayne", "Register");
         empArray[2] = new Employee(37, "Alan", "Grill");
         empArray[3] = new Employee(38, "Danny", "Drive Thru");
         empArray[4] = new Employee(39, "Chad", "Manager");
         
         Run run = new Run();
         System.out.println("Before Employees were sorted: ");
         run.printData();
         
         run.sortAge(run.empArray);
         System.out.println("Employees are sorted by age: ");
         run.printData();
         
         run.sortName(run.empArray);
         System.out.println("Employees are sorted by name: ");
         run.printData();
         
         run.sortPosition(run.empArray);
         System.out.println("Employees are sorted by Position: ");
         run.printData();
         
       
       long endTime = System.nanoTime();
       System.out.println("Took "+((endTime - startTime)/1000000) + "ms to excute Java Program"); 
       
       //System.out.println("It took 395ns to sort by Age");
       //System.out.println("It took 395ns to sort by Name ");
       
	}
}
