import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class Driver {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayStack as = new ArrayStack(10000000);

        as.push(10);

        as.push(11);

        as.push(1);

        as.push(0);

        as.push(5);
    

        System.out.println(as.size()); // this should print 5

        System.out.println(as.pop()); // this should print 5
    
		long length = 1000000L;
		long start = System.nanoTime();
		{
			
			for (int index = 0; index < length; index++) {
				 as.push(index);
				 System.out.println(as.size());
			}
			long end = System.nanoTime();
			long difference = end - start;
			System.out.println(
					"Execution Time in Nanoseconds " + TimeUnit.NANOSECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Microseconds "
					+ TimeUnit.MICROSECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Milliseconds "
					+ TimeUnit.MILLISECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Seconds " + TimeUnit.SECONDS.convert(difference, TimeUnit.NANOSECONDS));
		}
        
        
	}

}
