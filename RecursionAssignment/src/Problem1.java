
public class Problem1 {

	public static String ReverseWord(String word){
	
		if(word.length() == 0)
		     return "";
		   return word.charAt(word.length() - 1) + ReverseWord(word.substring(0,word.length()-1));
	
	}
	public static void main(String[] args) {
		
		System.out.println(ReverseWord("Hello World"));
}}
