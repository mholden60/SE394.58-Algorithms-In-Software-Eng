
import java.util.concurrent.TimeUnit;

public class Run {

	public static void main(String args[]) {

		String str = "My name is Mat";
		String temp = "" + "\n";
		String finalString = "";

		for (int i = str.length() - 1; i >= 0; i--) {
			temp += i != 0 ? str.charAt(i) : str.charAt(i) + " ";
			if (str.charAt(i) == ' ' || i == 0) {
				for (int j = temp.length() - 1; j >= 0; j--) {
					finalString += temp.charAt(j);
				}
				temp = "\n";
			}
		}
		System.out.print(finalString);

		// *************************************WITH
		// FUNCTIONS************************************************************
		// String str1="Funtions with is This";
		// String words[]=str1.split(" ");
		// for(int i=words.length-1;i>=0;i--){
		// System.out.print(words[i]+" "+"\n");
		// }
		// System.out.println("");
		// ****************************************************************************************************************
		long length = 1000000000L;
		long start = System.nanoTime();
		{
			for (int index = 0; index < length; index++) {
			}
			long end = System.nanoTime();
			long difference = end - start;
			System.out.println(
					"Execution Time in Nanoseconds " + TimeUnit.NANOSECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Microseconds "
					+ TimeUnit.MICROSECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Milliseconds "
					+ TimeUnit.MILLISECONDS.convert(difference, TimeUnit.NANOSECONDS));
			System.out.println("Execution Time in Seconds " + TimeUnit.SECONDS.convert(difference, TimeUnit.NANOSECONDS));
		}
	}
}
